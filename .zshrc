# completion
autoload -Uz compinit
compinit -d ~/.cache/zcompdump-$ZSH_VERSION
setopt extendedGlob # extended globbing (eg '*' and '?')
_comp_options+=(globdots) # dot files
zstyle ':completion:*' menu select # arrowkeys
zstyle ':completion:*' matcher-list 'm:{a-z}={A-Z}' # lower to uppercase
zstyle ':completion:*:warnings' format 'no matches'

# history
HISTSIZE=2000
SAVEHIST=2000
HISTFILE=~/.cache/zsh_history
setopt HIST_IGNORE_ALL_DUPS
setopt HIST_SAVE_NO_DUPS
setopt HIST_REDUCE_BLANKS

# aliases
alias ip="ip -color"
alias ipy="ip -br a"
alias cp="cp --interactive --verbose"
alias mv="mv --interactive --verbose"
alias rm="rm --verbose"
alias ln='ln --interactive --verbose'
alias chmod="chmod --changes"
alias chown="chown --changes"
alias mkdir="mkdir --verbose"
alias suvim="sudoedit"
alias ls='ls -Fv --human-readable --color=auto --group-directories-first'
alias la='ls -a'
alias ll='ls -al'
alias cls='clear; pwd; ls -al'
alias dir='dir --color=auto'
alias vdir='vdir --color=auto'
alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'
alias youtube-dl='yt-dlp'
alias base-yt-dlp='yt-dlp --ignore-errors --extract-audio --audio-format mp3 --embed-thumbnail --embed-metadata --parse-metadata ":(?P<meta_description>)" --parse-metadata ":(?P<meta_synopsis>)" --parse-metadata ":(?P<meta_purl>)" --parse-metadata ":(?P<meta_date>)"'
alias music-dl='base-yt-dlp --parse-metadata ":(?P<album>)" --parse-metadata ":(?P<track_number>)"'
alias playlist-dl='base-yt-dlp --yes-playlist --parse-metadata "playlist_title:%(album)s" --parse-metadata "playlist_index:%(track_number)s"'
alias glog="git log --oneline --graph --color --all --decorate"
alias gitls="git ls-files"
alias gs='git status'
alias py='python3'
alias svenv='source venv/bin/activate'
alias zshreload="source $ZDOTDIR/.zshrc"
alias zshhist="vim +$ ~/.cache/zsh_history"
alias biosreboot="systemctl reboot --firmware-setup"
alias untar='tar -zxvf'
alias whatislove='echo "baby don\`t hurt me"'

if [ "$USER" = "peter" ]; then # peter special
	alias pacman-orphans='sudo pacman -Qtdq'
	alias pacman-remove-orphans='sudo pacman -Rns $(pacman -Qtdq)' # remove orphan packages
fi
if [ "$USER" = "christoph" ]; then # christoph special
	alias ..='cd ..'
	alias ...='cd ../..'
	alias ....='cd ../../..'
	alias gac='git add . && git commit -m'
	set -o emacs
fi
if [ -f /usr/bin/flatpak ]; then
	if flatpak list | grep com.visualstudio.code >> /dev/null; then # flatpak
		alias code='flatpak run com.visualstudio.code'
	fi
fi

# load colors & style
autoload colors; colors
parse_git_branch() {
    git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ \1/'
}
setopt PROMPT_SUBST
PROMPT='%B%{$fg[red]%}[%{$fg[yellow]%}%n%{$fg[green]%}@%{$fg[blue]%}%M %{$fg[magenta]%}%~%{$fg[green]%}$(parse_git_branch)%{$fg[red]%}]%{$reset_color%}$%b '

# Vi mode is loaded by default
# ctrl + s				stops output to terminal
# ctrl + q				resumes output
autoload -Uz up-line-or-beginning-search down-line-or-beginning-search edit-command-line
zle -N up-line-or-beginning-search
zle -N down-line-or-beginning-search
zle -N edit-command-line
bindkey "^U"		backward-kill-line			# ctrl + u
bindkey "^K"		kill-whole-line				# ctrl + k
bindkey "^A"		beginning-of-line			# ctrl + a
bindkey "^[[H"		beginning-of-line       		# Home
bindkey "^E"		end-of-line				# ctrl + e
bindkey "^[[F"		end-of-line             		# End
bindkey "^D"		delete-char				# ctrl + d
bindkey "^[[3~"		delete-char				# Delete
bindkey "^P"		up-line-or-history			# ctrl + p
bindkey "^[[5~"		up-line-or-history			# PgUp
bindkey "^N"		down-line-or-history			# ctrl + n
bindkey "^[[6~"		down-line-or-history			# PgDn
bindkey "^[[Z"		reverse-menu-complete			# Shift Tab
bindkey "^[[1;5C"	forward-word				# Ctrl + right
bindkey "^[[1;5D"	backward-word				# Ctrl + left
bindkey "^R"		history-incremental-search-backward	# ctrl + r
bindkey "^[[A"		up-line-or-beginning-search		# $key[Up]
bindkey "^[[B"		down-line-or-beginning-search		# $key[Down]
bindkey -M vicmd v	edit-command-line			# ctrl + x + ctrl + e (edit in $EDITOR)
bindkey "^X^E"		edit-command-line

source $ZDOTDIR/zsh-plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source $ZDOTDIR/zsh-plugins/zsh-autosuggestions/zsh-autosuggestions.plugin.zsh
