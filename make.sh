#/bin/sh
mkdir -p ~/.config ~/.ssh
ln -sf ~/.dotfiles/.zshenv ~/.zshenv
if [ "$USER" = "peter" ]; then
	mkdir -p ~/.config/gtk-3.0 ~/.config/foot ~/.config/i3 ~/.config/sway
	ln -sf ~/.dotfiles/.gitconfig ~/.gitconfig
	ln -sf ~/.dotfiles/ssh/config ~/.ssh/config
	ln -sf ~/.dotfiles/config/gtk-3.0/settings.ini ~/.config/gtk-3.0/settings.ini
	ln -sf ~/.dotfiles/config/foot/foot.ini ~/.config/foot/foot.ini
	ln -sf ~/.dotfiles/config/i3/config ~/.config/i3/config
	ln -sf ~/.dotfiles/config/sway/config ~/.config/sway/config
	ln -sf ~/.dotfiles/.xsessionrc ~/.xsessionrc
fi
chmod 700 ~/.ssh
chmod 600 ~/.ssh/*

dotfiles_make_plugin() {
	if [ -d ~/.dotfiles/zsh-plugins/$1 ];
	then
		cd ~/.dotfiles/zsh-plugins/$1
		git pull
	else
		cd ~/.dotfiles/zsh-plugins
		git clone --depth 1 $2;
	fi
}
dotfiles_make_plugin "zsh-syntax-highlighting" "https://github.com/zsh-users/zsh-syntax-highlighting.git"
dotfiles_make_plugin "zsh-autosuggestions" "https://github.com/zsh-users/zsh-autosuggestions.git"
