#!/bin/zsh
# .zshenv - runs on every zsh instance
export skip_global_compinit="1"
## Environment variables
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_CACHE_HOME="$HOME/.cache"
export XDG_CONFIG_HOME="$HOME/.config"
if [[ ! -o norcs ]]; then # in Interactive shells
	export ZDOTDIR="$HOME/.dotfiles"
	export PATH=$PATH":$HOME/.local/bin"
	export PATH=$PATH":$ZDOTDIR/bin"
	export EDITOR="vim"
	export BROWSER="firefox"
	export WGETRC="$HOME/.config/wgetrc"
	export VIMINIT='source $MYVIMRC'
	export MYVIMRC="$ZDOTDIR/.vimrc"
	if [ -f /usr/bin/apt ]; then
		# Debian fix for up-line-or-beginning-search
		export DEBIAN_PREVENT_KEYBOARD_CHANGES=yes
		# Debian add /sbin to PATH
		export PATH=$PATH":/sbin"
	fi
fi
unsetopt BEEP
