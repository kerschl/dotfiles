" import defaults
runtime defaults.vim
" set line numbers
set number
" fix background in foot
if &term == "foot"
 set background=dark
endif
